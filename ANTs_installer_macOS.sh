#!/bin/sh
# A script to compile ANTs for macOS
# K. Nemoto 10 Nov 2022

# prerequisite: git and homebrew

# Check OS
os=$(uname)

if [ $os != "Darwin" ]; then
  echo "This script is for macOS only."
  echo "exit"
  exit 1
fi


# Cmake
echo "Update Cmake"
pip3 install -U cmake

# Prepare working directory under $HOME
echo "Prepare ANTS directory under $HOME"
echo "If ANTS exists, rename to ANTS_prev"

cd $HOME

if [ -d ANTS ]; then
  mv ANTS ANTS_prev
fi

mkdir -p ANTS/{build,install}
cd ANTS
echo "Compiled on $(date +%Y-%m-%d)" > README
workingDir=${PWD}

# Compile and install ANTs
git clone https://github.com/ANTsX/ANTs.git

cd build

cmake \
    -DCMAKE_INSTALL_PREFIX=${workingDir}/install \
    ../ANTs 2>&1 | tee cmake.log
make -j 4 2>&1 | tee build.log

cd ANTS-build
make install 2>&1 | tee install.log


# PATH setting
your_shell=$(echo $SHELL | awk -F/ '{ print $NF }')
if  [ ${your_shell} = 'bash' ]; then
  profile="$HOME/.bash_profile"
elif  [ ${your_shell} = 'zsh' ]; then
  profile="$HOME/.zprofile"
fi

grep '$HOME/ANTS/install/bin' ${profile} > /dev/null
if [ $? -eq 1 ]; then
  echo "" >> ${profile}
  echo "#ANTs" >> ${profile}
  echo 'export ANTSPATH=$HOME/ANTS/install/bin' >> ${profile}
  echo 'export PATH=$PATH:$ANTSPATH' >> ${profile}
fi

echo "ANTs is installed"
echo "Please close and re-run the terminal to reflect PATH setting"

